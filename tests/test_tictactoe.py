import pandas as pd
import numpy as np
from neckar.tictactoe import Player, State

BOARD_ROWS = 3
BOARD_COLS = 3

def test_get_hash():
    p1 = Player("p1")
    p2 = Player("p2")
    st = State(p1, p2)

    assert isinstance(st.getHash(), str)


def test_winning_pattern_naive():
    p1 = Player("p1")
    p2 = Player("p2")
    st = State(p1, p2)
    assert st.winner() is None

    st.board = pd.DataFrame(np.full((BOARD_ROWS, BOARD_COLS), 'X'))
    assert st.winner() == 'X'

    temp_df = pd.DataFrame({'c0': ['X', 'X', 'X'], 'c1': ['-', '-', '-'], 'c2': ['-', '-', '-']})
    temp_df.columns = [0,1,2]
    st.board = temp_df
    assert st.winner() == 'X'

    temp_df = pd.DataFrame({'c0': ['X', 'O', 'X'], 'c1': ['-', '-', '-'], 'c2': ['-', '-', '-']})
    temp_df.columns = [0,1,2]
    st.board = temp_df
    assert st.winner() is None

    temp_df = pd.DataFrame({'c0': ['X', '-', 'X'], 'c1': ['-', 'X', '-'], 'c2': ['-', '-', 'X']})
    temp_df.columns = [0,1,2]
    st.board = temp_df
    assert st.winner() == 'X'

    temp_df = pd.DataFrame({'c0': ['X', 'O', 'X'], 'c1': ['-', 'O', '-'], 'c2': ['-', 'O', '-']})
    temp_df.columns = [0,1,2]
    st.board = temp_df
    assert st.winner() == 'O'

    temp_df = pd.DataFrame({'c0': ['X', 'O', 'O'], 'c1': ['-', 'O', '-'], 'c2': ['O', 'X', '-']})
    temp_df.columns = [0,1,2]
    st.board = temp_df
    assert st.winner() == 'O'

    st.board = pd.DataFrame(np.full((BOARD_ROWS, BOARD_COLS), 'O'))
    assert st.winner() == 'O'
    assert len(st.availablePositions()) == 0