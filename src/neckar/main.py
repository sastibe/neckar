import fire
from neckar.tictactoe import Player, State, HumanPlayer


class TicTacToe(object):

    def train(self, rounds=500):
        p1 = Player("p1")
        p2 = Player("p2")

        st = State(p1, p2)

        print("training...")
        st.play(rounds)
        p1.savePolicy()
        return None

    def play(self):
        p1 = Player("computer", exp_rate=0)
        p1.loadPolicy("policy_p1")
        p2 = HumanPlayer("human")
        st = State(p1, p2)
        
        st.play2()

class Pipeline(object):

    def __init__(self):
        self.tictactoe = TicTacToe()
        self.connectfour = None

def run():
    fire.Fire(Pipeline)
