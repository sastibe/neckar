
.. image:: https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold
    :alt: Project generated with PyScaffold
    :target: https://pyscaffold.org/

|

======
neckar
======


    neckar is a toy example for Reinforcement Learning, CLI via fire, CI/CD on gitlab in Python.

A thorough description of the package can be found here: https://www.sastibe.de/2022/07/a-toy-example-for-reinforcement-learning-cli-via-fire-ci/cd-on-gitlab-in-python/

.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.2.3. For details and usage
information on PyScaffold see https://pyscaffold.org/.
